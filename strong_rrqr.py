# Apply QRCP to make a first approximation, then try to permutate some column in the approximation 
# with columns from the residuals to increase the determinant (colume) of the approximation
# after applying truncated QR

import scipy.io as io
import numpy as np
import matplotlib.pyplot as plt
import scipy

##### QRCP #####

# Orthonormalize the first k columns of A using QRCP
def qrcp(A, k):
    result = np.copy(A)
    n = min(result.shape)
    for i in range(0,k):
        # Get index of column of maximum norm
        max_col = np.argmax(np.sqrt(np.sum(np.square(result[:,i:]), axis=0)))

        # Swap with leading column
        result[:,[i,i+max_col]] = result[:,[i+max_col,i]]

        # Perform QR on the 1st column of the submatrix
        result[i:,i:] = qr_householder(result[i:,i:])

    return result

def norm(x):
    return np.sqrt(np.sum(np.square(x)))

def sign(a):
    if a == 0:
        return 1
    else: return np.sign(a)

# Compute Householder matrix from v
def householder(v):
    v = np.copy(v).reshape(1,len(v))
    Hv = np.eye(max(v.shape)) - 2*(v*v.T)/np.sum(v*v)
    return Hv

# Othonormalize the first column of A using QR-Householder
def qr_householder(A):
    x = A[:,0]
    alpha = -sign(x[0])*norm(x)
    e1 = np.zeros(len(x))
    e1[0] = 1
    v = (x-alpha*e1)/norm(x-alpha*e1)
    Q = householder(v);
    return np.dot(Q,A)


###### STRONG RRQR ######

def volume_quotient(A,k):
    Ak_inv = np.linalg.inv(A[:k,:k])
    result = np.dot(Ak_inv,A[:k,k:])**2
    for i in range(0,k):
        for j in range(0,A.shape[1]-k):
            result[i,j] += (np.linalg.norm(Ak_inv[i,:])*np.linalg.norm(A[k:,k+j]))**2

    return np.sqrt(result)

def volume_quotient2(A,k):
    result = np.zeros((k,A.shape[1] - k))
    tmp = np.empty(A.shape)
    for i in range(0,k):
        for j in range(0,A.shape[1]-k):
            tmp = np.copy(A)
            tmp[:,i] = A[:,j+k]
            tmp = qr_householder_multiple(tmp,k)
            result[i,j] = np.linalg.det(tmp[:k,:k])/np.linalg.det(A[:k,:k])
    return result

# Orthonormalize the first k columns of A
# Without Column Pivoting
def qr_householder_multiple(A,k):
    result = np.copy(A)
    for i in range(0,k):
        result[i:,i:] = qr_householder(result[i:,i:])

    return result

# Use Strong RRQR algorithm
def strong_rrqr(A,k):

    result = np.copy(A)

    vol_quotient = volume_quotient(result,k)
    print("max is", np.max(vol_quotient))
    while np.max(vol_quotient) > 1:

        # Find columns to swap using vol_quotient = det(A11 * P_ij) / det(A11)
        # with P_ij permutation matrix
        best_candidate_index = np.argmax(vol_quotient)
        best_candidate_i = best_candidate_index // (A.shape[1] - k)
        best_candidate_j = best_candidate_index % (A.shape[1] - k)

        # Swap columns j+k and i of result
        result[:,[best_candidate_i,best_candidate_j+k]] = result[:,[best_candidate_j+k,best_candidate_i]]
        result = qr_householder_multiple(result,k)
        vol_quotient = volume_quotient(result,k)
        print("max is", np.max(vol_quotient))

    return result
        

###### MAIN ######

if __name__ == '__main__':
    sparseA = io.mmread("/Users/mbeauper/code/bcsstk01.mtx")
    A = scipy.sparse.csc_matrix.todense(sparseA)
    #A = np.arange(25).reshape(5,5).astype("float")

    k = 10
    result_qrcp = qrcp(A, k)

    _,vs_qrcp,_ = np.linalg.svd(result_qrcp[:k,:k])

    result_strong = strong_rrqr(result_qrcp, k)

    _,vs_strongrrqr,_ = np.linalg.svd(result_strong[:k,:k])
    _,vs_ori,_ = np.linalg.svd(A)

    plt.plot(vs_strongrrqr, 'bo', label="rank-k Strong RRQR singular values")
    plt.plot(vs_qrcp, 'r+', label="rank-k QRCP singular values")
    plt.plot(vs_ori[:k], label="A's k greatest singular values")

    plt.legend()

    plt.show()

