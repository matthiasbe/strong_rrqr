# This program applies the k-truncated QR decomposition to get a rectangular 
# matrix containing approximated singular values on the diagonal, 
# using Column Pivoting to increase the precision of the approximation


import scipy.io as io
import numpy as np
import matplotlib.pyplot as plt
import scipy

def qrcp(A, k):
    result = np.copy(A)
    n = min(result.shape)
    for i in range(0,k):
        # Get index of column of maximum norm
        max_col = np.argmax(np.sqrt(np.sum(np.square(result[:,i:]), axis=0)))

        # Swap with leading column
        result[:,[i,i+max_col]] = result[:,[i+max_col,i]]

        # Perform QR factorization
        result[i:,i:] = qr_householder(result[i:,i:])

    return result

def norm(x):
    return np.sqrt(np.sum(np.square(x)))

def sign(a):
    if a == 0:
        return 1
    else: return np.sign(a)

def householder(v):
    v = np.copy(v).reshape(1,len(v))
    Hv = np.eye(max(v.shape)) - 2*(v*v.T)/np.sum(v*v)
    return Hv

def qr_householder(A):
    x = A[:,0]
    alpha = -sign(x[0])*norm(x)
    e1 = np.zeros(len(x))
    e1[0] = 1
    v = (x-alpha*e1)/norm(x-alpha*e1)
    Q = householder(v);
    return np.dot(Q,A)

if __name__ == '__main__':
    sparseA = io.mmread("/Users/mbeauper/code/bcsstk01.mtx")
    A = scipy.sparse.csc_matrix.todense(sparseA)
    k = 10
    result = qrcp(A, k)

    plt.imshow(result)
    plt.colorbar()
    plt.show()

    _,s,_ = np.linalg.svd(result[:k,:k])
    plt.plot(s, label="k first approx singular values")
    _,s,_ = np.linalg.svd(result[k:,k:])
    plt.plot(np.array(range(k, len(A))), s, label="n-k last approx singular values")

    _,s,_ = np.linalg.svd(A)
    plt.plot(s, 'gx', label="real singular values")

    plt.legend()
    plt.show()

